package ru.tsc.apozdnov.tm.exception.user;

import ru.tsc.apozdnov.tm.exception.AbstractExeption;

public class AccessDeniedException extends AbstractExeption {

    public AccessDeniedException() {
        super("Error! You are not logged in. Please log in and try again...");
    }

}