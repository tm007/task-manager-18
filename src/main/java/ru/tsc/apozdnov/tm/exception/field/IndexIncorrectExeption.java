package ru.tsc.apozdnov.tm.exception.field;

public final class IndexIncorrectExeption extends AbstractFieldExeption {

    public IndexIncorrectExeption() {
        super("FAULT! Index is EMPTY!!!");
    }

}
