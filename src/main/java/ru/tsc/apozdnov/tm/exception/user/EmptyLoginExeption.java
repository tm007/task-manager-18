package ru.tsc.apozdnov.tm.exception.user;

public class EmptyLoginExeption extends AbstractUserExeption {

    public EmptyLoginExeption() {
        super("Error!!! Login is empty!");
    }

}
