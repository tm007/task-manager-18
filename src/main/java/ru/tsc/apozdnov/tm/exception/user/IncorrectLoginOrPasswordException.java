package ru.tsc.apozdnov.tm.exception.user;

public class IncorrectLoginOrPasswordException extends AbstractUserExeption {

    public IncorrectLoginOrPasswordException() {
        super("Error! Incorrect login or password entered. Please try again...");
    }

}