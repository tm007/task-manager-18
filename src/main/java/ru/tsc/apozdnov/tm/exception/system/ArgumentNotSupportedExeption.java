package ru.tsc.apozdnov.tm.exception.system;

import ru.tsc.apozdnov.tm.exception.AbstractExeption;

public final class ArgumentNotSupportedExeption extends AbstractExeption {

    public ArgumentNotSupportedExeption() {
        super("FAULT!! Argument Not Supported");
    }

    public ArgumentNotSupportedExeption(final String arg) {
        super("FAULT!!! Argument " + arg + " is not supported!");
    }

}
