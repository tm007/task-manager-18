package ru.tsc.apozdnov.tm.exception.entity;

public final class ProjectNotFoundExeption extends AbstractEntityNotFoundExeption {

    public ProjectNotFoundExeption() {
        super("FAULT!Project not found!!!");
    }

}
