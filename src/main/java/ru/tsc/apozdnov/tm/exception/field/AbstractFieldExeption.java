package ru.tsc.apozdnov.tm.exception.field;

import ru.tsc.apozdnov.tm.exception.AbstractExeption;

public class AbstractFieldExeption extends AbstractExeption {

    public AbstractFieldExeption() {
        super();
    }

    public AbstractFieldExeption(final String message) {
        super(message);
    }

    public AbstractFieldExeption(final String message, final Throwable cause) {
        super(message, cause);
    }

    public AbstractFieldExeption(final Throwable cause) {
        super(cause);
    }

    protected AbstractFieldExeption(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
