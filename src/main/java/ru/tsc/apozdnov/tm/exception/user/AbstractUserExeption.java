package ru.tsc.apozdnov.tm.exception.user;

import ru.tsc.apozdnov.tm.exception.AbstractExeption;

public class AbstractUserExeption extends AbstractExeption {

    public AbstractUserExeption() {
        super();
    }

    public AbstractUserExeption(final String message) {
        super(message);
    }

    public AbstractUserExeption(final String message, final Throwable cause) {
        super(message, cause);
    }

    public AbstractUserExeption(final Throwable cause) {
        super(cause);
    }

    protected AbstractUserExeption(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
