package ru.tsc.apozdnov.tm.exception.field;

public final class IdEmptyExeption extends AbstractFieldExeption {

    public IdEmptyExeption() {
        super("FAULT! Id is empty!!!");
    }

}
