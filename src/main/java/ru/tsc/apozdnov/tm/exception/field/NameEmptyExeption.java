package ru.tsc.apozdnov.tm.exception.field;

public final class NameEmptyExeption extends AbstractFieldExeption {

    public NameEmptyExeption() {
        super("FAULT!! Name is EMPTY");
    }

}
