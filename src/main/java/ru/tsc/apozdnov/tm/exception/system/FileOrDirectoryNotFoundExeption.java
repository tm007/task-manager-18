package ru.tsc.apozdnov.tm.exception.system;

import ru.tsc.apozdnov.tm.exception.AbstractExeption;

public final class FileOrDirectoryNotFoundExeption extends AbstractExeption {

    public FileOrDirectoryNotFoundExeption() {
        super("FAULT!! File or Directory does not exist!!!!");
    }

}
