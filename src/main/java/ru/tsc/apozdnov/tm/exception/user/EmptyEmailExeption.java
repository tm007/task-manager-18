package ru.tsc.apozdnov.tm.exception.user;

public class EmptyEmailExeption extends AbstractUserExeption {

    public EmptyEmailExeption() {
        super("Error!!! Email is empty !");
    }

}
