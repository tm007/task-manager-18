package ru.tsc.apozdnov.tm.exception.entity;

public final class TaskNotFoundExeption extends AbstractEntityNotFoundExeption {

    public TaskNotFoundExeption() {
        super("Fault!!! Task not found!!!");
    }

}
