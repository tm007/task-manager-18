package ru.tsc.apozdnov.tm.exception.user;

public class UserNotFoundExeption extends AbstractUserExeption {

    public UserNotFoundExeption() {
        super("Error!!! User not found !");
    }

}
