package ru.tsc.apozdnov.tm.service;

import ru.tsc.apozdnov.tm.api.service.IAuthService;
import ru.tsc.apozdnov.tm.api.service.IUserService;
import ru.tsc.apozdnov.tm.exception.user.AccessDeniedException;
import ru.tsc.apozdnov.tm.exception.user.EmptyLoginExeption;
import ru.tsc.apozdnov.tm.exception.user.EmptyPasswordExeption;
import ru.tsc.apozdnov.tm.exception.user.IncorrectLoginOrPasswordException;
import ru.tsc.apozdnov.tm.model.User;
import ru.tsc.apozdnov.tm.util.HashUtil;

public class AuthService implements IAuthService {

    private final IUserService userService;

    private String userId;

    public AuthService(final IUserService userService) {
        this.userService = userService;
    }

    @Override
    public void registry(String login, String password, String email) {
        userService.create(login, password, email);
    }

    @Override
    public void login(String login, String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginExeption();
        if (password == null || password.isEmpty()) throw new EmptyPasswordExeption();
        User user = userService.findByLogin(login);
        String hash = HashUtil.salt(password);
        if (!hash.equals(user.getPasswordHash())) throw new IncorrectLoginOrPasswordException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }

    @Override
    public String getUserId() {
        if (!isAuth()) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public User getUser() {
        if (!isAuth()) throw new AccessDeniedException();
        return userService.findById(userId);
    }

}