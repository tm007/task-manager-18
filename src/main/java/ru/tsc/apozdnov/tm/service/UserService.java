package ru.tsc.apozdnov.tm.service;

import ru.tsc.apozdnov.tm.api.repository.IUserRepository;
import ru.tsc.apozdnov.tm.api.service.IUserService;
import ru.tsc.apozdnov.tm.enumerated.RoleType;
import ru.tsc.apozdnov.tm.exception.field.IdEmptyExeption;
import ru.tsc.apozdnov.tm.exception.user.*;
import ru.tsc.apozdnov.tm.model.User;
import ru.tsc.apozdnov.tm.util.HashUtil;

import java.util.List;

public class UserService implements IUserService {

    private final IUserRepository userRepository;

    public UserService(final IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User create(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginExeption();
        if (isLoginExist(login)) throw new ExistLoginExeption();
        if (password == null || password.isEmpty()) throw new EmptyPasswordExeption();
        return userRepository.create(login, password);
    }

    @Override
    public User create(final String login, final String password, final String email) {
        if (login == null || login.isEmpty()) throw new EmptyLoginExeption();
        if (isLoginExist(login)) throw new ExistLoginExeption();
        if (password == null || password.isEmpty()) throw new EmptyPasswordExeption();
        if (isEmailExist(email)) throw new ExistEmailExeption();
        return userRepository.create(login, password, email);
    }

    @Override
    public User create(final String login, final String password, final RoleType role) {
        if (login == null || login.isEmpty()) throw new EmptyLoginExeption();
        if (isLoginExist(login)) throw new ExistLoginExeption();
        if (password == null || password.isEmpty()) throw new EmptyPasswordExeption();
        return userRepository.create(login, password, role);
    }

    @Override
    public User add(final User user) {
        if (user == null) throw new UserNotFoundExeption();
        return userRepository.add(user);
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User findById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyExeption();
        final User user = userRepository.findById(id);
        if (user == null) throw new UserNotFoundExeption();
        return user;
    }

    @Override
    public User findByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginExeption();
        final User user = userRepository.findByLogin(login);
        if (user == null) throw new UserNotFoundExeption();
        return user;
    }

    @Override
    public User findByEmail(final String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailExeption();
        final User user = userRepository.findByEmail(email);
        if (user == null) throw new UserNotFoundExeption();
        return user;
    }

    @Override
    public User remove(final User user) {
        if (user == null) throw new UserNotFoundExeption();
        return userRepository.remove(user);
    }

    @Override
    public User removeById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyExeption();
        final User user = findById(id);
        return remove(user);
    }

    @Override
    public User removeByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginExeption();
        final User user = findByLogin(login);
        return remove(user);
    }

    @Override
    public User removeByEmail(final String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailExeption();
        final User user = findByEmail(email);
        return remove(user);
    }

    @Override
    public User setPassword(final String id, final String password) {
        if (id == null || id.isEmpty()) throw new IdEmptyExeption();
        if (password == null || password.isEmpty()) throw new EmptyPasswordExeption();
        final User user = findById(id);
        user.setPasswordHash(HashUtil.salt(password));
        return user;
    }

    @Override
    public User userUpdate(final String id, final String firstName, final String lastName, final String middleName) {
        if (id == null || id.isEmpty()) throw new IdEmptyExeption();
        final User user = findById(id);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

    @Override
    public Boolean isLoginExist(final String login) {
        if (login == null || login.isEmpty()) return false;
        return userRepository.isLoginExist(login);
    }

    @Override
    public Boolean isEmailExist(final String email) {
        if (email == null || email.isEmpty()) return false;
        return userRepository.isEmailExist(email);
    }

}
