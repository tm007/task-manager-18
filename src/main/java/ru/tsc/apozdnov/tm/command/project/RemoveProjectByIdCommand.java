package ru.tsc.apozdnov.tm.command.project;

import ru.tsc.apozdnov.tm.model.Project;
import ru.tsc.apozdnov.tm.util.TerminalUtil;

public class RemoveProjectByIdCommand extends AbstractProjectCommand {

    @Override
    public String getName() {
        return "project-remove-by-id";
    }

    @Override
    public String getDescription() {
        return "Remove project by id.";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public void execute() {
        System.out.println("***** REMOVE PROJECT BY ID ****");
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        serviceLocator.getProjectTaskService().removeProjectById(projectId);
    }

}
