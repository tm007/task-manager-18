package ru.tsc.apozdnov.tm.command.project;

import ru.tsc.apozdnov.tm.model.Project;
import ru.tsc.apozdnov.tm.util.TerminalUtil;

public class ShowProjectByIndexCommand extends AbstractProjectCommand {

    @Override
    public String getName() {
        return "project-show-by-index";
    }

    @Override
    public String getDescription() {
        return "Show project by index.";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public void execute() {
        System.out.println("***** SHOW PROJECT BY INDEX ****");
        System.out.println("ENTER PROJECT INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        Project project = serviceLocator.getProjectService().findOneByIndex(index);
        showProject(project);
    }

}
