package ru.tsc.apozdnov.tm.command.system;

import ru.tsc.apozdnov.tm.api.service.ICommandService;
import ru.tsc.apozdnov.tm.command.AbstractCommand;

public abstract class AbstractSystemCommand extends AbstractCommand {

    public ICommandService getCommandService() {
        return getServiceLocator().getCommandService();
    }

}
