package ru.tsc.apozdnov.tm.api.service;

import ru.tsc.apozdnov.tm.model.User;

public interface IAuthService {

    void registry(String login, String password, String email);

    void login(String login, String password);

    void logout();

    boolean isAuth();

    String getUserId();

    User getUser();

}