package ru.tsc.apozdnov.tm.api.model;

import ru.tsc.apozdnov.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
